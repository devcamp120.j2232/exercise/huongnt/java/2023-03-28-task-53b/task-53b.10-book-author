public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo đối tượng author1
        Author author1 = new Author("Lan Nguyen", "Lan@gmail.com", 'f');
        // khởi tạo đối tượng author2
        Author author2 = new Author("Jacky", "jacky@gmail.com", 'm');

        // in ra console
        System.out.println("Author 1");
        System.out.println(author1.toString());
        System.out.println("Author 2");
        System.out.println(author2.toString());

        // khởi tạo đối tượng book1
        Book book1 = new Book("Gone with wind", author1, 200000);
        // khởi tạo đối tượng book2
        Book book2 = new Book("Harry Porter", author2, 250000, 2);

        System.out.println("Book 1");
        System.out.println(book1.toString());
        System.out.println("Book 2");
        System.out.println(book2.toString());

    }
}
