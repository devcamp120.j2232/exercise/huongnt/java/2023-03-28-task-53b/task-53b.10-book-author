public class Book {
    String name;
    Author author;
    double price;
    int qty = 0;

    // phương thức khởi tạo có 3 tham số
    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    // phương thức khởi tạo có 4 tham số
    public Book(String name, Author author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    //getter
    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }
 //setter
    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

       // in ra console
       @Override
       public String toString() {
        //trường hợp dùng getName mà name bị null thì bị báo lỗi, cách tránh lỗi này là sử dụng chỉ this.author
           return String.format("Book[name =" + this.name + ", Author[name = " + this.author.getName() + ", email = " + this.author.getEmail() + ", gender = " + this.author.getGender() + "], price = " + this.price + ", qty = " + this.qty + "]");
       }
   
    
    
}
