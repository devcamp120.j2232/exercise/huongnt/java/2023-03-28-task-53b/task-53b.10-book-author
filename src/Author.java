public class Author {
    String name;
    String email;
    char gender;

    // phương thức khởi tạo có tham số
    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // in ra console
    @Override
    public String toString() {
        return String.format("(Name = %s) (email =%s)(gender = %s)", name, email, gender);
    }

}
